from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Profile(models.Model):
    firstname = models.CharField(max_length=20, unique=False)
    lastname = models.CharField(max_length=20, unique=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    user_id = models.IntegerField()
    weight = models.DecimalField(decimal_places=2, max_digits=5)
    age = models.IntegerField()
    height = models.DecimalField(decimal_places=2, max_digits=3)
    bmi = models.DecimalField(decimal_places=2, max_digits=4)
    gender = models.CharField(max_length=1, unique=False)
    target_calories = models.IntegerField()


    class Meta:
        ordering = ['-created_on']

    def get_user(self):
        return User.objects.get(id=self.user_id)

    def set_user(self, user):
        self.user_id = user.id

    def __str__(self):
        return self.firstname + self.lastname