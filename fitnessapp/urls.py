from rest_framework import routers
from .api import ProfileViewSet

router = routers.DefaultRouter()
router.register('fitnessapi/profile', ProfileViewSet, 'profiles')
urlpatterns = router.urls