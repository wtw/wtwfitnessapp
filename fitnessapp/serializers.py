from rest_framework import serializers
from fitnessapp.models import Profile
from django.contrib.auth.models import User


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = '__all__'

    def validate_user_id(self, value):
        if User.objects.filter(id=value).count() == 0:
            raise serializers.ValidationError(
                "Invalid User id {}".format(value))
        return value

    def validate_gender(self, value):
        if value != 'M' and value != 'F':
            raise serializers.ValidationError(
                "Invalid Gender {}".format(value))
        return value

    def validate_current_user_id(self, current_user, target_user):
        if int(current_user) != target_user:
            # raise serializers.ValidationError("Only the author of this comment can update or delete it!")
            raise serializers.ValidationError(
                "Can't access other user's profile")
        return
